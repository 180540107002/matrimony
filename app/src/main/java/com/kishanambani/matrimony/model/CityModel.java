package com.kishanambani.matrimony.model;

import java.io.Serializable;

public class CityModel implements Serializable {
    String cityName;
    int cityCode;

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public int getCityCode() {
        return cityCode;
    }

    public void setCityCode(int cityCode) {
        this.cityCode = cityCode;
    }
}
