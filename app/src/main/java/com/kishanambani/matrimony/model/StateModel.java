package com.kishanambani.matrimony.model;

import java.io.Serializable;

public class StateModel implements Serializable {
    int id;
    String stateName;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    @Override
    public String toString() {
        return "StateModel{" +
                "id=" + id +
                ", stateName='" + stateName + '\'' +
                '}';
    }
}
