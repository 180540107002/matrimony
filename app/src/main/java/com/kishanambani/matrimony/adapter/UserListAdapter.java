package com.kishanambani.matrimony.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.kishanambani.matrimony.R;
import com.kishanambani.matrimony.database.TblUserDetail;
import com.kishanambani.matrimony.model.UserDetailModel;

import java.util.ArrayList;

import static com.kishanambani.matrimony.R.drawable.favourite_selected;
import static com.kishanambani.matrimony.R.drawable.favourite_unselected;

public class UserListAdapter extends BaseAdapter {
    Context context;
    ArrayList<UserDetailModel> userDetailModels;

    public UserListAdapter(Context context, ArrayList<UserDetailModel> userDetailModels) {
        this.context = context;
        this.userDetailModels = userDetailModels;
    }

    @Override
    public int getCount() {
        return userDetailModels.size();
    }

    @Override
    public Object getItem(int i) {
        return userDetailModels.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        View view1 = LayoutInflater.from(context).inflate(R.layout.list_view_design, null);
        TblUserDetail tblUserDetail = new TblUserDetail(context);
        TextView tvGender = view1.findViewById(R.id.tvActGender);
        TextView tvName = view1.findViewById(R.id.tvActName);
        TextView tvHobby = view1.findViewById(R.id.tvActHobby);
        ImageButton tbtnFavourite = view1.findViewById(R.id.btnActFavourite);
        Log.d("UserList::", "" + userDetailModels);
        tvName.setText(userDetailModels.get(position).getUserName());
        if (String.valueOf(userDetailModels.get(position).getGender()).substring(0, 1).equals("F")) {
            tvGender.setBackground(ContextCompat.getDrawable(context, R.drawable.gender_background_circle_female));
        } else {
            tvGender.setBackground(ContextCompat.getDrawable(context, R.drawable.gender_background_circle_male));
        }
        if (userDetailModels.get(position).getFavourites().equals("1")) {
            tbtnFavourite.setImageResource(favourite_selected);
        } else {
            tbtnFavourite.setImageResource(favourite_unselected);
        }
        tvGender.setText(String.valueOf(userDetailModels.get(position).getGender()).substring(0, 1));
        tvHobby.setText("Hobby:-" + userDetailModels.get(position).getHobbies());
        tbtnFavourite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (userDetailModels.get(position).getFavourites().equals("0")) {
                    tbtnFavourite.setImageResource(favourite_selected);
                    tblUserDetail.updateUserFavourite(userDetailModels.get(position).getEmail(), "1");
                } else if (userDetailModels.get(position).getFavourites().equals("1")) {
                    tbtnFavourite.setImageResource(favourite_unselected);
                    tblUserDetail.updateUserFavourite(userDetailModels.get(position).getEmail(), "0");
                }
            }
        });
        return view1;
    }
}
