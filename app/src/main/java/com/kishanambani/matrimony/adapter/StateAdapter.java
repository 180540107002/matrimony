package com.kishanambani.matrimony.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.kishanambani.matrimony.R;
import com.kishanambani.matrimony.model.StateModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class StateAdapter extends BaseAdapter {

    Context context;
    ArrayList<StateModel> stateModelArrayList;

    public StateAdapter(Context context, ArrayList<StateModel> stateModelArrayList) {
        this.context = context;
        this.stateModelArrayList = stateModelArrayList;
    }

    @Override
    public int getCount() {
        return stateModelArrayList.size();
    }

    @Override
    public Object getItem(int i) {
        return stateModelArrayList.get(i).getId();
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View view1 = view;
        ViewHolder viewHolder;
        if (view1 == null) {
            view1 = LayoutInflater.from(context).inflate(R.layout.spinner_row, null);
            viewHolder = new ViewHolder(view1);
            view1.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view1.getTag();
        }
        viewHolder.tvSpinnerTextView.setText(stateModelArrayList.get(i).getStateName());
        return view1;
    }

    static class ViewHolder {
        @BindView(R.id.tvSpinnerTextView)
        TextView tvSpinnerTextView;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
