package com.kishanambani.matrimony.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.kishanambani.matrimony.R;
import com.kishanambani.matrimony.model.CityModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class CityAdapter extends BaseAdapter {

    Context context;
    ArrayList<CityModel> cityModelArrayList;

    public CityAdapter(Context context, ArrayList<CityModel> cityModelArrayList) {
        this.context = context;
        this.cityModelArrayList = cityModelArrayList;
    }

    @Override
    public int getCount() {
        return cityModelArrayList.size();
    }

    @Override
    public Object getItem(int i) {
        return cityModelArrayList.get(i).getCityName();
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View view1 = view;
        ViewHolder viewHolder;
        if (view1 == null) {
            view1 = LayoutInflater.from(context).inflate(R.layout.spinner_row, null);
            viewHolder = new ViewHolder(view1);
            view1.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view1.getTag();
        }
        viewHolder.tvSpinnerTextView.setText(cityModelArrayList.get(i).getCityName());
        return view1;
    }

    class ViewHolder {
        @BindView(R.id.tvSpinnerTextView)
        TextView tvSpinnerTextView;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
