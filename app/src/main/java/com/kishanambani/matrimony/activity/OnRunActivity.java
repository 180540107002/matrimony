package com.kishanambani.matrimony.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.kishanambani.matrimony.R;

public class OnRunActivity extends AppCompatActivity {
    Button btnLogIn;
    TextView tvSignin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_onrun);
        init();
        btnLogIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(OnRunActivity.this, LogInActivity.class);
                startActivity(intent);
            }
        });
        tvSignin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent2 = new Intent(OnRunActivity.this, SignInActivity.class);
                startActivity(intent2);
            }
        });
    }

    public void init() {
        btnLogIn = findViewById(R.id.btnLogIn);
        tvSignin = findViewById(R.id.tvSignIn);
    }
}