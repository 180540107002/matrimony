package com.kishanambani.matrimony.activity;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.kishanambani.matrimony.R;
import com.kishanambani.matrimony.adapter.CityAdapter;
import com.kishanambani.matrimony.adapter.StateAdapter;
import com.kishanambani.matrimony.database.TblCityNames;
import com.kishanambani.matrimony.database.TblStateNames;
import com.kishanambani.matrimony.database.TblUserDetail;
import com.kishanambani.matrimony.model.CityModel;
import com.kishanambani.matrimony.model.StateModel;

import java.util.ArrayList;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SignInActivity extends AppCompatActivity {
    @BindView(R.id.etActFirstName)
    EditText etActFirstName;
    @BindView(R.id.etActMiddleName)
    EditText etActMiddleName;
    @BindView(R.id.etActLastName)
    EditText etActLastName;
    @BindView(R.id.etActDateOfBirth)
    EditText etActDateOfBirth;
    @BindView(R.id.etActPhoneNumber)
    EditText etActPhoneNumber;
    @BindView(R.id.etActEmail)
    EditText etActEmail;
    @BindView(R.id.rbActMale)
    RadioButton rbActMale;
    @BindView(R.id.rbActFemale)
    RadioButton rbActFemale;
    @BindView(R.id.rbActOthers)
    RadioButton rbActOthers;
    @BindView(R.id.rgActGenderGroup)
    RadioGroup rgActGenderGroup;
    @BindView(R.id.etActHobbies)
    EditText etActHobbies;
    @BindView(R.id.spActSelectState)
    Spinner spActSelectState;
    @BindView(R.id.spActSelectCity)
    Spinner spActSelectCity;
    @BindView(R.id.btnActRegisterMe)
    Button btnActRegisterMe;
    @BindView(R.id.etActPasswordSignIn)
    EditText etActPasswordSignIn;

    CityAdapter cityAdapter;
    StateAdapter stateAdapter;

    ArrayList<CityModel> cityModelArrayList = new ArrayList<>();
    ArrayList<StateModel> stateModelArrayList = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sigin);
        ButterKnife.bind(this);
        init();
        btnActRegisterMe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isValid()) {
                    isValidThen();
                }
            }
        });
    }

    public void isValidThen() {
        String userName = etActFirstName.getText().toString() + " " + etActMiddleName.getText().toString() + " " + etActLastName.getText().toString();
        String userDOB = etActDateOfBirth.getText().toString();
        String userPhonenumber = etActPhoneNumber.getText().toString();
        String userEmail = etActEmail.getText().toString();
        int id = rgActGenderGroup.getCheckedRadioButtonId();
        RadioButton rbSelected = findViewById(id);
        String userGender = rbSelected.getText().toString();
        String userHobbies = etActHobbies.getText().toString();
        String userState = stateModelArrayList.get((Integer) spActSelectState.getSelectedItem()).getStateName();
        String userCity = spActSelectCity.getSelectedItem().toString();
        String userPassword = etActPasswordSignIn.getText().toString();
        String favourite = "0";
        long isAdded = new TblUserDetail(SignInActivity.this).insertUserDetail(userName, userPhonenumber, userEmail, userGender, userHobbies, userState, userCity, userPassword, userDOB, favourite);
        if (isAdded > 0) {
            Toast.makeText(SignInActivity.this, "User Sucessfully added!", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(SignInActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
        }
    }

    @OnClick(R.id.etActDateOfBirth)
    public void onEtActDateOfBirthClicked() {
        final Calendar newCalendar = Calendar.getInstance();
        DatePickerDialog datePickerDialog = new DatePickerDialog(
                SignInActivity.this, (DatePicker datePicker, int year, int month, int day) -> {
            etActDateOfBirth.setText(String.format("%02d", day) + "/" + String.format("%02d", (month + 1)) + "/" + year);
        }, newCalendar.get(Calendar.YEAR),
                newCalendar.get(Calendar.MONTH),
                newCalendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();
    }

    public void init() {
        stateModelArrayList.addAll(new TblStateNames(SignInActivity.this).getState());
        stateAdapter = new StateAdapter(SignInActivity.this, stateModelArrayList);
        Log.d("State_List", "" + stateModelArrayList);
        spActSelectState.setAdapter(stateAdapter);
        spActSelectCity.setEnabled(false);
        spActSelectCity.setClickable(false);
        spActSelectState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                int stateCode = (int) spActSelectState.getSelectedItem();
                if (stateCode > 0) {
                    cityModelArrayList.clear();
                    cityModelArrayList.addAll(new TblCityNames(SignInActivity.this).getCity(stateCode));
                    cityAdapter = new CityAdapter(SignInActivity.this, cityModelArrayList);
                    spActSelectCity.setAdapter(cityAdapter);
                    spActSelectCity.setEnabled(true);
                    spActSelectCity.setClickable(true);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }

    public boolean isValid() {
        boolean isValid = true, isToast = false;
        String valueForToast = "PLease Enter Your ";
        if (etActFirstName.getText().toString().length() == 0) {
            etActFirstName.setError("Please enter your firstname");
            isValid = false;
        }
        if (etActMiddleName.getText().toString().length() == 0) {
            etActMiddleName.setError("Please enter your middlename");
            isValid = false;
        }
        if (etActLastName.getText().toString().length() == 0) {
            etActLastName.setError("Please enter your lastname");
            isValid = false;
        }
        if (etActDateOfBirth.getText().toString().length() == 0) {
            etActDateOfBirth.setError("Please select your date of birth");
        }
        if (etActEmail.getText().toString().length() == 0) {
            etActEmail.setError("Please enter your email");
        } else {
            String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
            if (!etActEmail.getText().toString().matches(emailPattern)) {
                etActEmail.setError("Please enter valid email");
            }
        }
        int id = rgActGenderGroup.getCheckedRadioButtonId();
        if (id == -1) {
            valueForToast += "Gender and ";
            isToast = true;
            isValid = false;
        }
        if (spActSelectState.getSelectedItem().toString().length() == 0) {
            valueForToast += "State and ";
            isValid = false;
            isToast = true;
        } else {
            if (spActSelectCity.getSelectedItem().toString().length() == 0) {
                valueForToast += "City and ";
                isValid = false;
                isToast = true;
            }
        }

        if (etActPasswordSignIn.getText().toString().length() == 0) {
            etActPasswordSignIn.setError("Please enter your password");
        }
        if (isToast) {
            Toast.makeText(SignInActivity.this, valueForToast, Toast.LENGTH_SHORT).show();
        }
        return isValid;
    }
}
