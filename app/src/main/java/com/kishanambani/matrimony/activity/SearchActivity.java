package com.kishanambani.matrimony.activity;

import android.os.Bundle;
import android.widget.ListView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.kishanambani.matrimony.R;
import com.kishanambani.matrimony.adapter.UserListAdapter;
import com.kishanambani.matrimony.model.UserDetailModel;

import java.util.ArrayList;
import java.util.Collection;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SearchActivity extends AppCompatActivity {
    @BindView(R.id.lvActDisplayCandidates)
    ListView lvActDisplayCandidates;

    ArrayList<UserDetailModel> userDetailModels = new ArrayList<>();
    UserListAdapter userListAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        ButterKnife.bind(this);
        callingAdapter();
    }

    public void callingAdapter() {
        userDetailModels.addAll((Collection<? extends UserDetailModel>) getIntent().getSerializableExtra("USERDETAIL"));
        userListAdapter = new UserListAdapter(SearchActivity.this, userDetailModels);
        lvActDisplayCandidates.setAdapter(userListAdapter);
    }
}
