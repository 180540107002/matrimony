package com.kishanambani.matrimony.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.kishanambani.matrimony.R;
import com.kishanambani.matrimony.database.MyDatabase;
import com.kishanambani.matrimony.database.TblUserDetail;
import com.kishanambani.matrimony.model.UserDetailModel;

import java.util.ArrayList;

public class LogInActivity extends AppCompatActivity {
    EditText etEmail, etPassword;
    Button btnLogIn;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        init();
        btnLogIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isValid()) {
                    isValidThen();
                }
            }
        });
    }
    public void init() {
        new MyDatabase(LogInActivity.this).getReadableDatabase();
        etEmail = findViewById(R.id.etActEmailLoginScreen);
        etPassword = findViewById(R.id.etActPasswordLoginScreen);
        btnLogIn = findViewById(R.id.btnActLogInScreen);
    }
    public boolean isValid() {
        boolean isValid = true;
        String enteredEmail = etEmail.getText().toString();
        TblUserDetail tblUserDetail = new TblUserDetail(LogInActivity.this);
        ArrayList<UserDetailModel> userDetailModels = new ArrayList<>();
        userDetailModels.addAll(tblUserDetail.getParticularUserList(enteredEmail));
        Log.d("RETRIVED_DATABASE::", "" + userDetailModels);
        if (userDetailModels.size() < 1) {
            etEmail.setError("Please Enter Valid Email");
            isValid = false;
        } else if (!userDetailModels.get(0).getPassword().equals(etPassword.getText().toString())) {
            etPassword.setError("Please Enter Valid Password");
            isValid = false;
        }
        return isValid;
    }

    public void isValidThen() {
        String enteredEmail = etEmail.getText().toString();
        TblUserDetail tblUserDetail = new TblUserDetail(LogInActivity.this);
        ArrayList<UserDetailModel> userDetailModels = new ArrayList<>();
        userDetailModels.addAll(tblUserDetail.getParticularUserList(enteredEmail));
        Intent intent = new Intent(LogInActivity.this, UserLoggedInActivity.class);
        intent.putExtra("HERE THE GIFT", userDetailModels);
        startActivity(intent);
    }
}
