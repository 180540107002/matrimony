package com.kishanambani.matrimony.activity;

import android.os.Bundle;
import android.widget.ListView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.kishanambani.matrimony.R;
import com.kishanambani.matrimony.adapter.FavouriteAdapter;
import com.kishanambani.matrimony.model.UserDetailModel;

import java.util.ArrayList;
import java.util.Collection;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FavouriteActivity extends AppCompatActivity {
    @BindView(R.id.lvActFavouriteDisplay)
    ListView lvActFavouriteDisplay;

    ArrayList<UserDetailModel> userDetailModels = new ArrayList<>();
    FavouriteAdapter favouriteAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favourite);
        ButterKnife.bind(this);
        callFavouriteAdapter();
    }

    public void callFavouriteAdapter() {
        ArrayList<UserDetailModel> tempUserList = new ArrayList<>();
        tempUserList.addAll((Collection<? extends UserDetailModel>) getIntent().getSerializableExtra("USERDETAIL_FAVOURITE"));
        userDetailModels.clear();

        int count = 0;
        for (int i = 0; i < tempUserList.size(); i++) {
            if (tempUserList.get(i).getFavourites().equals("1")) {
                userDetailModels.add(tempUserList.get(i));
            }
        }
        favouriteAdapter = new FavouriteAdapter(FavouriteActivity.this, userDetailModels);
        lvActFavouriteDisplay.setAdapter(favouriteAdapter);
    }
}
