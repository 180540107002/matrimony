package com.kishanambani.matrimony.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;

import com.google.android.material.navigation.NavigationView;
import com.kishanambani.matrimony.R;
import com.kishanambani.matrimony.database.TblUserDetail;
import com.kishanambani.matrimony.model.UserDetailModel;

import java.util.ArrayList;
import java.util.Collection;

import butterknife.BindView;
import butterknife.ButterKnife;

public class UserLoggedInActivity extends AppCompatActivity {
    DrawerLayout menuDrawer;
    @BindView(R.id.tvActLoggedUsername)
    TextView tvActLoggedUsername;
    @BindView(R.id.tvActLoggedEmail)
    TextView tvActLoggedEmail;
    @BindView(R.id.tvActLoggedState)
    TextView tvActLoggedState;
    @BindView(R.id.tvActLoggedCity)
    TextView tvActLoggedCity;
    @BindView(R.id.btnActLogOut)
    Button btnActLogOut;
    @BindView(R.id.btnActDeActivate)
    Button btnActDeActivate;
    @BindView(R.id.btnActSearch)
    ImageButton btnActSearch;
    @BindView(R.id.btnActFavourite)
    ImageButton btnActFavourite;
    @BindView(R.id.svActUserScrollView)
    ScrollView svActUserScrollView;
    @BindView(R.id.nvActNavigation)
    NavigationView nvActNavigation;
    @BindView(R.id.dlActDrawerLayout)
    DrawerLayout dlActDrawerLayout;

    ArrayList<UserDetailModel> receivedArrayList = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_loggedin);
        ButterKnife.bind(this);
        setNavigationDrawer();
        setLoggedInScreen();
        btnActSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchButtonClicked();
            }
        });
        btnActLogOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent1 = new Intent(UserLoggedInActivity.this, OnRunActivity.class);
                UserLoggedInActivity.this.startActivity(intent1);
            }
        });
        btnActFavourite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                favouriteButtonClicked();
            }
        });
    }

    public void searchButtonClicked() {
        ArrayList<UserDetailModel> userDetailModels = new ArrayList<>();
        TblUserDetail tblUserDetail = new TblUserDetail(UserLoggedInActivity.this);
        userDetailModels.addAll(tblUserDetail.getUserList());
        Log.d("here your mistake:", "" + userDetailModels.get(1) + "      " + userDetailModels.get(0));
        Intent intent = new Intent(UserLoggedInActivity.this, SearchActivity.class);
        intent.putExtra("USERDETAIL", userDetailModels);
        UserLoggedInActivity.this.startActivity(intent);
    }

    public void favouriteButtonClicked() {
        ArrayList<UserDetailModel> userDetailModels = new ArrayList<>();
        TblUserDetail tblUserDetail = new TblUserDetail(UserLoggedInActivity.this);
        userDetailModels.addAll(tblUserDetail.getUserList());
        Intent intent2 = new Intent(UserLoggedInActivity.this, FavouriteActivity.class);
        intent2.putExtra("USERDETAIL_FAVOURITE", userDetailModels);
        UserLoggedInActivity.this.startActivity(intent2);
    }

    public void setLoggedInScreen() {
        receivedArrayList.addAll((Collection<? extends UserDetailModel>) getIntent().getSerializableExtra("HERE THE GIFT"));
        tvActLoggedUsername.setText(receivedArrayList.get(0).getUserName());
        tvActLoggedEmail.setText(receivedArrayList.get(0).getEmail());
        tvActLoggedCity.setText(receivedArrayList.get(0).getCity());
        tvActLoggedState.setText(receivedArrayList.get(0).getState());
    }

    public void setNavigationDrawer() {
        menuDrawer = findViewById(R.id.dlActDrawerLayout);
        NavigationView navigationView = findViewById(R.id.nvActNavigation);
        navigationView.setNavigationItemSelectedListener(menuItem -> {
            int itemId = menuItem.getItemId();
            if (itemId == R.id.first) {
                Intent intent4 = new Intent(UserLoggedInActivity.this, ContactUsActivity.class);
                startActivity(intent4);
                return true;
            } else if (itemId == R.id.second) {
                Intent intent5 = new Intent(UserLoggedInActivity.this, EditProfileActivity.class);
                startActivity(intent5);
                return true;
            }
            return false;
        });
    }
}
