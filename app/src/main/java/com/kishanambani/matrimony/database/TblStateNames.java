package com.kishanambani.matrimony.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.kishanambani.matrimony.model.StateModel;

import java.util.ArrayList;

public class TblStateNames extends MyDatabase {
    public static final String TABLE_NAME = "TblStateNames";
    public static final String STATE_ID = "id";
    public static final String STATE_NAME = "state";

    public TblStateNames(Context context) {
        super(context);
    }

    public ArrayList<StateModel> getState() {
        SQLiteDatabase db = getReadableDatabase();
        ArrayList<StateModel> stateList = new ArrayList<>();
        String query = "SELECT * FROM " + TABLE_NAME;
        Cursor cursor = db.rawQuery(query, null);
        StateModel stateModel1 = new StateModel();
        stateModel1.setStateName("Select from below option");
        stateList.add(stateModel1);
        cursor.moveToFirst();
        for (int i = 0; i < cursor.getCount(); i++) {
            StateModel stateModel = new StateModel();
            Log.d("InForLoop:-", "" + cursor.getString(cursor.getColumnIndex(STATE_NAME)));
            stateModel.setId(cursor.getInt(cursor.getColumnIndex(STATE_ID)));
            stateModel.setStateName(cursor.getString(cursor.getColumnIndex(STATE_NAME)));
            Log.d("state_model", "" + stateModel);
            stateList.add(stateModel);
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
        Log.d("StateList ::", "" + stateList);
        return stateList;
    }
}
