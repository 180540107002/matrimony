package com.kishanambani.matrimony.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.kishanambani.matrimony.model.UserDetailModel;

import java.util.ArrayList;

public class TblUserDetail extends MyDatabase {
    public static final String TBL_USER_DETAIL = "TblUserDetail";
    public static final String USER_NAME = "UserName";
    public static final String PHONE_NUMBER = "PhoneNumber";
    public static final String EMAIL = "Email";
    public static final String GENDER = "Gender";
    public static final String HOBBIES = "Hobbies";
    public static final String STATE = "State";
    public static final String CITY = "City";
    public static final String PASSWORD = "Password";
    public static final String DATE_OF_BIRTH = "DateOfBirth";
    public static final String FAVOURITE = "Favourites";

    public TblUserDetail(Context context) {
        super(context);
    }

    public long insertUserDetail(String name, String phoneNumber, String email, String gender, String hobbies, String state, String city, String password, String dateOfBirth, String favourites) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(USER_NAME, name);
        cv.put(PHONE_NUMBER, phoneNumber);
        cv.put(EMAIL, email);
        cv.put(GENDER, gender);
        cv.put(HOBBIES, hobbies);
        cv.put(STATE, state);
        cv.put(CITY, city);
        cv.put(PASSWORD, password);
        cv.put(DATE_OF_BIRTH, dateOfBirth);
        cv.put(FAVOURITE, favourites);
        long insertedID = db.insert(TBL_USER_DETAIL, null, cv);
        db.close();
        return insertedID;
    }

    public ArrayList<UserDetailModel> getParticularUserList(String callerEmail) {
        SQLiteDatabase db = getReadableDatabase();
        ArrayList<UserDetailModel> userList = new ArrayList<>();
        String query = "SELECT * FROM " + TBL_USER_DETAIL + " WHERE " + EMAIL + " == ?";
        Cursor cursor = db.rawQuery(query, new String[]{callerEmail});
        cursor.moveToFirst();
        for (int i = 0; i < cursor.getCount(); i++) {
            UserDetailModel userDetailModel = new UserDetailModel();
            userDetailModel.setUserName(cursor.getString(cursor.getColumnIndex(USER_NAME)));
            userDetailModel.setPassword(cursor.getString(cursor.getColumnIndex(PHONE_NUMBER)));
            userDetailModel.setEmail(cursor.getString(cursor.getColumnIndex(EMAIL)));
            userDetailModel.setGender(cursor.getString(cursor.getColumnIndex(GENDER)));
            userDetailModel.setHobbies(cursor.getString(cursor.getColumnIndex(HOBBIES)));
            userDetailModel.setState(cursor.getString(cursor.getColumnIndex(STATE)));
            userDetailModel.setCity(cursor.getString(cursor.getColumnIndex(CITY)));
            userDetailModel.setPassword(cursor.getString(cursor.getColumnIndex(PASSWORD)));
            userDetailModel.setDateOfBirth(cursor.getString(cursor.getColumnIndex(DATE_OF_BIRTH)));
            userDetailModel.setFavourites(cursor.getString(cursor.getColumnIndex(FAVOURITE)));
            userList.add(userDetailModel);
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
        return userList;
    }

    public ArrayList<UserDetailModel> getUserList() {
        SQLiteDatabase db = getReadableDatabase();
        ArrayList<UserDetailModel> userList = new ArrayList<>();
        String query = "SELECT * FROM " + TBL_USER_DETAIL;
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        for (int i = 0; i < cursor.getCount(); i++) {
            UserDetailModel userDetailModel = new UserDetailModel();
            userDetailModel.setUserName(cursor.getString(cursor.getColumnIndex(USER_NAME)));
            userDetailModel.setPhoneNumber(cursor.getString(cursor.getColumnIndex(PHONE_NUMBER)));
            userDetailModel.setEmail(cursor.getString(cursor.getColumnIndex(EMAIL)));
            userDetailModel.setGender(cursor.getString(cursor.getColumnIndex(GENDER)));
            userDetailModel.setHobbies(cursor.getString(cursor.getColumnIndex(HOBBIES)));
            userDetailModel.setState(cursor.getString(cursor.getColumnIndex(STATE)));
            userDetailModel.setCity(cursor.getString(cursor.getColumnIndex(CITY)));
            userDetailModel.setPassword(cursor.getString(cursor.getColumnIndex(PASSWORD)));
            userDetailModel.setDateOfBirth(cursor.getString(cursor.getColumnIndex(DATE_OF_BIRTH)));
            userDetailModel.setFavourites(cursor.getString(cursor.getColumnIndex(FAVOURITE)));
            userList.add(userDetailModel);
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
        return userList;
    }

    public void updateUserFavourite(String changedEmail, String favouriteValue) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(FAVOURITE, favouriteValue);
        db.update(TBL_USER_DETAIL, cv, EMAIL + " =?", new String[]{changedEmail});
        db.close();
    }
}
