package com.kishanambani.matrimony.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.kishanambani.matrimony.model.CityModel;

import java.util.ArrayList;

public class TblCityNames extends MyDatabase {
    public static final String TABLE_NAME = "TblCityNames";
    public static final String CITY_NAME = "CityName";
    public static final String CITY_CODE = "CityCode";
    public static final String STATE_CODE = "StateCode";

    public TblCityNames(Context context) {
        super(context);
    }

    public ArrayList<CityModel> getCity(int stateCode) {
        ArrayList<CityModel> cityList = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        String query = "SELECT " + CITY_NAME + "," + CITY_CODE + " FROM " + TABLE_NAME + " WHERE " + STATE_CODE + " =?";
        Cursor cursor = db.rawQuery(query, new String[]{"" + stateCode});
        cursor.moveToFirst();
        for (int i = 0; i < cursor.getCount(); i++) {
            CityModel cityModel = new CityModel();
            cityModel.setCityName(cursor.getString(cursor.getColumnIndex(CITY_NAME)));
            cityModel.setCityCode(cursor.getInt(cursor.getColumnIndex(CITY_CODE)));
            cityList.add(cityModel);
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
        return cityList;
    }
}
